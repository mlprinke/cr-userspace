/*
 * rtckptdriver.cpp
 *
 *  Created on: Jun 17, 2018
 *      Author: mprinke
 */
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include "rt_ckpt_driver.h"
#define DEVICE_FILE_NAME "/dev/creplay"

rt_ckpt_driver::rt_ckpt_driver() {
	// TODO Auto-generated constructor stub
	file_desc = open(DEVICE_FILE_NAME, 0);
	if (file_desc < 0) {
		printf ("Can't open device file: %s\n",
			DEVICE_FILE_NAME);
		exit(-1);
	}
	error = 0;

}

rt_ckpt_driver::~rt_ckpt_driver() {
	// TODO Auto-generated destructor stub
}

void rt_ckpt_driver::ioctl_pid_msg(pid_t pid)
{
  int ret_val;
  struct pid_only pid_only;
  pid_only.pid = pid;
  ret_val = ioctl(file_desc, RT_CKPT_IOCTL_PID_MSG, &pid_only);

  if (ret_val < 0) {
    printf ("RT_CKPT_IOCTL_PID_MSG failed:%d\n", ret_val);
    error = true;
    //exit(-1);
  }
}

void rt_ckpt_driver::ioctl_get_regs_msg(int num_pids,pid_t * pid,struct pid_list *pid_list)
{
  int ret_val;
  //while(num_pids > 0)
  //{
	  pid_list->size = num_pids > 16 ? 16 : num_pids;
	  memcpy(pid_list->pids,pid,sizeof(pid_t)*pid_list->size);

	  ret_val = ioctl(file_desc, RT_CKPT_IOCTL_GET_REGS, pid_list);

	  if (ret_val < 0) {
		printf ("RT_CKPT_IOCTL_GET_REGS failed:%d\n", ret_val);
	    error = true;
		//exit(-1);
	  }
	  //pid += pid_list->size;
	  //num_pids -= pid_list->size;
  //}
}

int rt_ckpt_driver::ioctl_pop_msg(pid_t pid,struct checkpoint_msg * buf,int size_buf,int set_ckpt_inprogress)
{
  int ret_val;
  struct pid_pop pid_pop;
  pid_pop.size = size_buf;
  pid_pop.buf = buf;
  pid_pop.return_size = 0;
  pid_pop.pid = pid;
  pid_pop.set_ckpt_inprogress = set_ckpt_inprogress;
  ret_val = ioctl(file_desc, RT_CKPT_IOCTL_POP_QUEUE, &pid_pop);

  if (ret_val < 0) {
    printf ("RT_CKPT_IOCTL_POP_QUEUE failed:%d\n", ret_val);
    error = true;
   // exit(-1);
  }

  /*if(pid_pop.return_size > 0)
  {
	  printf ("RT_CKPT_IOCTL_POP_QUEUE got :%d\n", pid_pop.return_size);
  }
  */
  return pid_pop.return_size;
}

void rt_ckpt_driver::ioctl_monitor_msg(pid_t pid, char *message)
{
  int ret_val;
  struct pid_file_message pid_file_message;
  memset((void*)&pid_file_message,0, sizeof(struct pid_file_message));
  strcpy(pid_file_message.file_path,message);
  pid_file_message.pid = pid;
  ret_val = ioctl(file_desc, RT_CKPT_IOCTL_MONITOR_MSG, &pid_file_message);

  if (ret_val < 0) {
    printf ("ioctl_set_msg failed:%d\n", ret_val);
    error = true;
    //exit(-1);
  }
}

void rt_ckpt_driver::ioctl_replay_msg(char *message)
{
  int ret_val;

  ret_val = ioctl(file_desc, RT_CKPT_IOCTL_REPLAY_MSG, message);

  if (ret_val < 0) {
    printf ("ioctl_set_msg failed:%d\n", ret_val);
    error = true;
    //exit(-1);
  }
}

void rt_ckpt_driver::ioctl_checkpoint_msg(pid_t pid, char *message)
{
	int ret_val;
	struct pid_file_message pid_file_message;
	memset((void*)&pid_file_message,0, sizeof(struct pid_file_message));
	strcpy(pid_file_message.file_path,message);
	pid_file_message.pid = pid;
	ret_val = ioctl(file_desc, RT_CKPT_IOCTL_CHECKPOINT_MSG, &pid_file_message);

  if (ret_val < 0) {
    printf ("ioctl_checkpoint_msg failed:%d\n", ret_val);
    error = true;
    //exit(-1);
  }
}


void rt_ckpt_driver::ioctl_checkpoint_stall_msg(pid_t pid, char *message)
{
	int ret_val;
	struct pid_file_message pid_file_message;
	memset((void*)&pid_file_message,0, sizeof(struct pid_file_message));
	strcpy(pid_file_message.file_path,message);
	pid_file_message.pid = pid;
	ret_val = ioctl(file_desc, RT_CKPT_IOCTL_CHECKPOINT_STALL_MSG, &pid_file_message);

  if (ret_val < 0) {
    printf ("ioctl_checkpoint_stall_msg failed:%d\n", ret_val);
    error = true;
    //exit(-1);
  }
}


void rt_ckpt_driver::ioctl_write_pte_msg(struct pte_write_message *message)
{
  int ret_val;
  ret_val = ioctl(file_desc, RT_CKPT_IOCTL_WRITE_PTE_MSG, message);

  if (ret_val < 0) {
    printf ("ioctl_write_pte_msg RT_CKPT_IOCTL_:%d\n", ret_val);
    error = true;
    //exit(-1);
  }
}

void rt_ckpt_driver::ioctl_read_pte(struct pte_write_message * message)
{
  int ret_val;
  ret_val = ioctl(file_desc, RT_CKPT_IOCTL_READ_PTE_MSG, message);

  if (ret_val < 0) {
    printf ("ioctl_read_pte RT_CKPT_IOCTL_:%d\n", ret_val);
    error = true;
    //exit(-1);
  }
}

void rt_ckpt_driver::ioctl_write_vma_msg(struct vma_desc_message *message)
{
  int ret_val;
  ret_val = ioctl(file_desc, RT_CKPT_IOCTL_WRITE_VMA_MSG, message);

  if (ret_val < 0) {
    printf ("ioctl_write_vma_msg failed:%d\n", ret_val);
    error = true;
    //exit(-1);
  }
}

void rt_ckpt_driver::ioctl_vma_dump_msg(struct vma_dump *message)
{
  int ret_val;
  ret_val = ioctl(file_desc, RT_CKPT_IOCTL_VMA_DUMP_MSG, message);

  if (ret_val < 0) {
	printf ("ioctl_vma_dump failed:%d\n", ret_val);
    error = true;
	//exit(-1);
  }
}


void rt_ckpt_driver::ioctl_get_log(struct pid_log *pid_log)
{
  int ret_val;
  ret_val = ioctl(file_desc, RT_CKPT_IOCTL_GET_LOG, pid_log);

  if (ret_val < 0) {
	printf ("ioctl_get_log failed:%d\n", ret_val);
    error = true;
	//exit(-1);
  }
}


