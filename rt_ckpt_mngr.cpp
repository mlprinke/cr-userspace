/*
 * rt_ckpt_mngr.cpp
 *
 *  Created on: Jun 17, 2018
 *      Author: mprinke
 */


#include <unistd.h>     /* exit */


#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/syscall.h>
#include <linux/ioctl.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <dirent.h>
#include <cstring>

#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <emmintrin.h>
//#define DEBUG 1

#ifdef DEBUG
#define debug(f_, ...) printf((f_), ##__VA_ARGS__)
#else
#define debug(f_, ...)
#endif

#include "rt_ckpt_mngr.h"

int rt_ckpt_mngr::keepRunning = 0;



static inline unsigned long _tsc_read(void)
{
	union {
		struct  {
			unsigned int lo;
			unsigned int hi;
		};
		unsigned long  value;
	}  rv;

	/* rdtsc & cpuid clobbers eax, ebx, ecx and edx registers */
	__asm__ volatile (/* serialize */
		"xorl %%eax,%%eax;\n\t"
		"cpuid;\n\t"
		:
		:
		: "%eax", "%ebx", "%ecx", "%edx"
		);
	/*
	 * We cannot use "=A", since this would use %rax on x86_64 and
	 * return only the lower 32bits of the TSC
	 */
	__asm__ volatile ("rdtsc" : "=a" (rv.lo), "=d" (rv.hi));


	return rv.value;
}

char * checkpoint_event_type_strings[25] =
	{"CKPT_EVENT_UNKNOWN",
	"CKPT_EVENT_PAGE_FAULT_ENTRY",
	"CKPT_EVENT_PAGE_FAULT_ANON",
	"CKPT_EVENT_PAGE_FAULT_DIRTY",
	"CKPT_EVENT_PAGE_FAULT_FINISH",
	"CKPT_EVENT_PAGE_FAULT_STALL",
	"CKPT_EVENT_PAGE_FAULT_RESUME",
	"CKPT_EVENT_SWAP_IN",
	"CKPT_EVENT_SWAP_OUT",
	"CKPT_EVENT_SMP_FUNC_CALL_ISSUE",
	"CKPT_EVENT_SMP_FUNC_CALL_START",
	"CKPT_EVENT_SMP_FUNC_CALL_FINISH",
	"CKPT_EVENT_SMP_FUNC_CALL_COMPLETE",
	"CKPT_EVENT_POP_PAGE",
	"CKPT_EVENT_PAGE_WALK_START",
	"CKPT_EVENT_PAGE_WALK_COMPLETE",
	"CKPT_EVENT_MEMREMAP_START",
	"CKPT_EVENT_MEMREMAP_COMPLETE",
	"CKPT_EVENT_PAGE_COPY_START",
	"CKPT_EVENT_PAGE_COPY_COMPLETE",
	"CKPT_EVENT_CLEAR_DIRTY",
	"CKPT_EVENT_CHECKPOINT_START",
	"CKPT_EVENT_CHECKPOINT_COMPLETE",
	"CKPT_EVENT_THREAD_HALTED",
	"CKPT_EVENT_TRHEAD_UNQUEUED"} ;


void  INThandler(int sig)
{
	printf("Caught kill event");
	rt_ckpt_mngr::keepRunning = 0;
}

rt_ckpt_mngr::rt_ckpt_mngr() {
	// TODO Auto-generated constructor stub
	rt_ckpt_mngr::keepRunning = 1;


	vma_tree = RBTreeCreate(rt_ckpt_mngr::cmp_vma_save,
						 NULL,//free,
						 NULL,
						 (void (*)(const void*))printf,
						 (void (*)(void*))printf);

	pte_tree = RBTreeCreate(rt_ckpt_mngr::cmp_pte_save,
						 NULL,//free,
						 NULL,
						 (void (*)(const void*))printf,
						 (void (*)(void*))printf);

	checkpoint_id = 0;
	pte_changes_empty = 1;
	vma_changes_empty = 1;
	checkpoint_verify = 0;
	checkpoint_period = CKPT_TIME_THRESHOLD_US;
	pid_log.size = DEFUALT_CKPT_EVENT_QUEUE_SIZE;
	pid_log.buf = (struct rt_ckpt_driver::checkpoint_event_queue_entry*)calloc(sizeof(struct rt_ckpt_driver::checkpoint_event_queue_entry),DEFUALT_CKPT_EVENT_QUEUE_SIZE);

}

rt_ckpt_mngr::~rt_ckpt_mngr() {
	// TODO Auto-generated destructor stub
	RBTreeDestroy(vma_tree);
	RBTreeDestroy(pte_tree);
}



int
rt_ckpt_mngr::cmp_vma_save (const void * a, const void * b) {
    struct vma_save *va = (struct vma_save *) a;
    struct vma_save *vb = (struct vma_save *) b;
    //printf("VMA cmp(A) at 0x%llX-0x%llX\n",va->vm_start,va->vm_end);
    //printf("VMA cmp(B) at 0x%llX-0x%llX\n",vb->vm_start,vb->vm_end);
    int result = (vb->vm_start >= va->vm_start) & (vb->vm_start < va->vm_end);
    //printf(" %d & %d = %d\n ",(vb->vm_start >= va->vm_start) , (vb->vm_start < va->vm_end),result);
    return result ? 0 : vb->vm_start >= va->vm_start ? 1 : -1;
}

int
rt_ckpt_mngr::cmp_pte_save (const void * a, const void * b) {
    struct pte_save *va = (struct pte_save *) a;
    struct pte_save *vb = (struct pte_save *) b;
    return (va->addr == vb->addr) ? 0 : vb->addr > va->addr ? 1 : -1;
}


int rt_ckpt_mngr::read_base_checkpoint_file(char * base_file,char * ckpt_file)
{

	rb_red_blk_node * node = NULL;
	struct vma_save * found_vma = NULL;
	struct pte_save * found_pte = NULL;
	struct
	{
		unsigned long vm_end;
		unsigned long vm_start;
		unsigned long vm_flags;
		unsigned long vm_page_prot;
		unsigned long addr;
		unsigned long end;
	}vma_dump_temp_vars;

	char cbuf[1024];
	unsigned long long zero = 0, rpte = 0;
	int i  = 0;

	unsigned long raddr,rend;
	sprintf(cbuf,"%s.vma",base_file);
	FILE * f = fopen(cbuf,"r");
	if(!f)
	{
		printf("Could not open %s for reading\n",cbuf);
		return 0;
	}
	printf("Loading %s\n",cbuf);
	struct rt_ckpt_driver::vma_dump * temp_vma_dump = (struct rt_ckpt_driver::vma_dump *) malloc(sizeof(struct rt_ckpt_driver::vma_dump) + sizeof(struct rt_ckpt_driver::vma_desc_message) * 1);
	temp_vma_dump->return_size = 1;
	temp_vma_dump->size = 1;
	struct pte_save temp_pte;
	struct rt_ckpt_driver::checkpoint_msg  temp_ckpt_buf;
	while(1)
	{
		if(!fread((char*)&vma_dump_temp_vars,sizeof(unsigned long)*6,1,f)) goto close_file;
		temp_vma_dump->vmas[i].vm_start = vma_dump_temp_vars.vm_start;
		temp_vma_dump->vmas[i].vm_end = vma_dump_temp_vars.vm_end;
		temp_vma_dump->vmas[i].vm_flags = vma_dump_temp_vars.vm_flags;
		temp_vma_dump->vmas[i].vm_page_prot = vma_dump_temp_vars.vm_page_prot;
		add_vma_to_tree(temp_vma_dump);
		raddr = vma_dump_temp_vars.addr;
		rend = vma_dump_temp_vars.end;

		for (; raddr != rend; raddr += RT_CKPT_PAGE_SIZE) {

			if(!fread((char*)&rpte,sizeof(unsigned long long),1,f)) goto close_file;

			if (!rpte)
			{
				//debug("Replay skipping PTE 0x%lx\n",raddr);
				continue;
			}


			//debug("Replaying pte for 0x%lX :Replay 0x%lx\n",raddr,rpte);
			if(!fread(temp_ckpt_buf.data, RT_CKPT_PAGE_SIZE,1,f)) goto close_file;
			temp_ckpt_buf.vma_address = raddr;
			add_pte_to_tree(&temp_ckpt_buf, 1);

		}

	}
close_file:
	fclose(f);


	struct vma_save temp_vma;

	sprintf(cbuf,"%s.vma",ckpt_file);
	FILE * file = fopen(cbuf,"r");
	if(file)
	{
		printf("Loading %s\n",cbuf);
		while(fread(&temp_vma,sizeof(struct vma_save),1,file))
		{
			temp_vma_dump->vmas[i].vm_start = temp_vma.vm_start;
			temp_vma_dump->vmas[i].vm_end = temp_vma.vm_end;
			temp_vma_dump->vmas[i].vm_flags = temp_vma.vm_flags;
			temp_vma_dump->vmas[i].vm_page_prot = temp_vma.vm_page_prot;
			add_vma_to_tree(temp_vma_dump);
		}

		fclose(file);
	}
	else
	{
		printf("Could not open %s for reading\n",cbuf);
	}


	sprintf(cbuf,"%s.pte",ckpt_file);
	file = fopen(cbuf,"r");
	if(file)
	{
		printf("Loading %s\n",cbuf);
		while(fread(&temp_pte,sizeof(struct pte_save),1,file))
		{
			temp_ckpt_buf.vma_address = temp_pte.addr;
			memcpy(temp_ckpt_buf.data,temp_pte.data,RT_CKPT_PAGE_SIZE);
			add_pte_to_tree(&temp_ckpt_buf, 1);
		}


		fclose(file);
	}
	else
	{
		printf("Could not open %s for reading\n",cbuf);
	}
}
int keepRunning = 1;

int rt_ckpt_mngr::load_dump_file(char * filename)
{
	rb_red_blk_node * node = NULL;
	struct vma_save * found_vma = NULL;
	struct pte_save * found_pte = NULL;
	struct
	{
		unsigned long vm_end;
		unsigned long vm_start;
		unsigned long vm_flags;
		unsigned long vm_page_prot;
		unsigned long addr;
		unsigned long end;
	}vma_dump_temp_vars;


	unsigned long long zero = 0, rpte = 0;
	int i  = 0;

	unsigned long raddr,rend;

	FILE * f = fopen(filename,"r");
	if(!f)
	{
		printf("Could not open %s for reading\n",filename);
		return 0;
	}
	printf("Loading %s\n",filename);
	struct rt_ckpt_driver::vma_dump * temp_vma_dump = (struct rt_ckpt_driver::vma_dump *) malloc(sizeof(struct rt_ckpt_driver::vma_dump) + sizeof(struct rt_ckpt_driver::vma_desc_message) * 1);
	temp_vma_dump->return_size = 1;
	temp_vma_dump->size = 1;
	struct pte_save temp_pte;
	struct rt_ckpt_driver::checkpoint_msg  temp_ckpt_buf;
	while(1)
	{
		if(!fread((char*)&vma_dump_temp_vars,sizeof(unsigned long)*6,1,f)) goto close_file;
		temp_vma_dump->vmas[i].vm_start = vma_dump_temp_vars.vm_start;
		temp_vma_dump->vmas[i].vm_end = vma_dump_temp_vars.vm_end;
		temp_vma_dump->vmas[i].vm_flags = vma_dump_temp_vars.vm_flags;
		temp_vma_dump->vmas[i].vm_page_prot = vma_dump_temp_vars.vm_page_prot;
		add_vma_to_tree(temp_vma_dump);
		raddr = vma_dump_temp_vars.addr;
		rend = vma_dump_temp_vars.end;

		for (; raddr != rend; raddr += RT_CKPT_PAGE_SIZE) {

			if(!fread((char*)&rpte,sizeof(unsigned long long),1,f)) goto close_file;

			if (!rpte)
			{
				//debug("Replay skipping PTE 0x%lx\n",raddr);
				continue;
			}


			//debug("Replaying pte for 0x%lX :Replay 0x%lx\n",raddr,rpte);
			if(!fread(temp_ckpt_buf.data, RT_CKPT_PAGE_SIZE,1,f)) goto close_file;
			temp_ckpt_buf.vma_address = raddr;
			add_pte_to_tree(&temp_ckpt_buf, 1);

		}

	}
close_file:
	fclose(f);

}

int rt_ckpt_mngr::squash_checkpoints(int start, int stop)
{

	printf("Squashing %d from %d to %d\n",dump_file_base,start,stop);
	if(stop < start)
		return 0;
	char buf[1024];
	char fbuf[8096];
	struct rt_ckpt_driver::vma_dump * temp_vma_dump = (struct rt_ckpt_driver::vma_dump *) malloc(sizeof(struct rt_ckpt_driver::vma_dump) + sizeof(struct rt_ckpt_driver::vma_desc_message) * 1);
	temp_vma_dump->return_size = 1;
	temp_vma_dump->size = 1;
	if(checkpoint_verify)
	{
		char buf[1024];
		sprintf(buf,"%s.pre_dump",dump_file_base);
		load_dump_file(buf);
		sprintf(buf,"%s.pre_dump.verify",dump_file_base);
		char * old  = dump_file_base;
		dump_file_base = buf;
		write_checkpoint(true);
		dump_file_base = old;
	}
	for(int i = start; i <= stop; i++)
	{

		if(checkpoint_verify)
		{
			char buf[1024];
			sprintf(buf,"%s.%d.verify_dump",dump_file_base,i);
			load_dump_file(buf);
		}

		struct vma_save * temp_vma = (struct vma_save * )fbuf;

		sprintf(buf,"%s.%d.vma",dump_file_base,i);
		FILE * vma_file = fopen(buf,"rb");
		int loop_i = 0;
		if(vma_file)
		{

			printf("Opened %s\n",buf);
			printf("reading temp_vma %d\n",loop_i);
			while(fread(fbuf,sizeof(struct vma_save),1,vma_file))
			{
				temp_vma_dump->vmas[0].vm_start = temp_vma->vm_start;
				temp_vma_dump->vmas[0].vm_end = temp_vma->vm_end;
				temp_vma_dump->vmas[0].vm_flags = temp_vma->vm_flags;
				temp_vma_dump->vmas[0].vm_page_prot = temp_vma->vm_page_prot;
				add_vma_to_tree(temp_vma_dump,checkpoint_verify);
				loop_i++;
			}

			fclose(vma_file);
		}

		struct pte_save * temp_pte = (struct pte_save * )fbuf;
		struct rt_ckpt_driver::checkpoint_msg  temp_ckpt_buf;

		sprintf(buf,"%s.%d.pte",dump_file_base,i);
		FILE * pte_file = fopen(buf,"rb");
		loop_i = 0;
		if(pte_file)
		{

			printf("Opened %s\n",buf);
			while(fread(temp_pte,sizeof(struct pte_save),1,pte_file))
			{
				temp_ckpt_buf.vma_address = temp_pte->addr;
				printf("Verify Read 0x%llX\n", temp_pte->addr);
				memcpy(temp_ckpt_buf.data,temp_pte->data,RT_CKPT_PAGE_SIZE);
				add_pte_to_tree(&temp_ckpt_buf, 1,checkpoint_verify);
				loop_i++;
			}


			fclose(pte_file);
		}
		if(checkpoint_verify)
		{
			verify_checkpoint();
		}
	}
	char * old_dump_file_base = dump_file_base;
	sprintf(buf,"%s.squash.%d",old_dump_file_base,start);
	checkpoint_id = stop;
	dump_file_base = buf;
	write_checkpoint(true);
	dump_file_base = old_dump_file_base;



}

int rt_ckpt_mngr::gettids(int pid, int * tid_buf)
{
	int i = 0;
	DIR *proc_dir;
	{
		char dirname[100];
		snprintf(dirname, sizeof dirname, "/proc/%d/task", pid);
		proc_dir = opendir(dirname);
	}

	if (proc_dir)
	{
		/* /proc available, iterate through tasks... */
		struct dirent *entry;
		while ((entry = readdir(proc_dir)) != NULL)
		{
			if(entry->d_name[0] == '.')
				continue;

			tid_buf[i++] = atoi(entry->d_name);

			/* ... (do stuff with tid) ... */
		}

		closedir(proc_dir);
	}
	else
	{
		return 0;
		/* /proc not available, act accordingly */
	}
	return i;
}
int rt_ckpt_mngr::init_dump_file(char * file_path)
{
	FILE * file = NULL;

	file = fopen(file_path,"w");
	if(file)
		fclose(file);
	else
		return -1;
}

int rt_ckpt_mngr::add_pte_to_tree(struct rt_ckpt_driver::checkpoint_msg * l_ckpt_buf, int num_entries,int verify)
{
	rb_red_blk_node * node = NULL;

	struct pte_save temp_pte;
	struct pte_save * found_pte = NULL;
	int i = 0;
	unsigned long long dirty = 0;
	while(i < num_entries)
	{

		node = RBExactQuery(pte_tree,&l_ckpt_buf[i]);
		if(node)
			found_pte = (struct pte_save*)node->key;
		else
			found_pte = NULL;

		if(!found_pte)
		{
			found_pte = (pte_save*)calloc(sizeof(struct pte_save),1);
			found_pte->addr = l_ckpt_buf[i].vma_address;
			found_pte->flags.raw = 0;
			found_pte->flags.bits.new_pte = 1;
			found_pte->dirty_bits = 0;
			if(verify)
				found_pte->flags.bits.new_unverified;
			memcpy(found_pte->data,l_ckpt_buf[i].data,RT_CKPT_PAGE_SIZE);
			debug("%s PTE NEW at 0x%llX\n",verify ? "Verify" : "", found_pte->addr);

			RBTreeInsert(pte_tree, found_pte,found_pte);
			pte_changes_empty = 0;
		}
		else
		{
			int pg_offset = 0;
			int bit_offset = 0;
			while( pg_offset < RT_CKPT_PAGE_SIZE)
			{
				/*__m128i vs, vt,v;
				vs = _mm_loadu_si128((__m128i*)&l_ckpt_buf[i].data[pg_offset]);
				vt = _mm_loadu_si128((__m128i*)&found_pte->data[pg_offset]);
				v = _mm_cmpeq_epi8(vs, vt);
				int cmp_result =  _mm_movemask_epi8 (v);*/
				int is_dirty = 0;
				for(int di = 0; di < 64/sizeof(long); di ++)
				{
					unsigned long* a = (unsigned long*)&l_ckpt_buf[i].data[pg_offset];
					unsigned long* b = (unsigned long*)&found_pte->data[pg_offset];
					is_dirty |= (a[di] != b[di]) ? 1: 0;
					if((a[di] != b[di]))
					{
						debug("%s miss match at [0x%llX] 0x%llX != 0x%llX \n",verify ? "Verify" : "", pg_offset + di,a[di], b[di]);
					}
				}
				dirty |= is_dirty;

				if(!verify)
				{
					found_pte->dirty_bits |= is_dirty << pg_offset;
					found_pte->flags.bits.dirty |= is_dirty  ? 1: 0;

					memcpy(&found_pte->data[pg_offset],&l_ckpt_buf[i].data[pg_offset],64);
					if(is_dirty)
					{
						debug("%s PTE DIRTY at 0x%llX flags %d dirty mask 0x%llX\n",verify ? "Verify" : "", found_pte->addr,found_pte->flags.raw,found_pte->dirty_bits);
						pte_changes_empty = 0;
					}
					is_dirty = 0;
				}




				pg_offset +=64;

			}
			if(verify)
			{
				if(found_pte->flags.bits.dirty)
					found_pte->flags.bits.verified_dirty = dirty ? 0 : 1;
				if(found_pte->flags.bits.clean)
					found_pte->flags.bits.verified_clean = dirty ? 0 : 1;
				if(found_pte->flags.bits.clean)
					found_pte->flags.bits.verified_new = dirty ? 0 : 1;
				found_pte->flags.bits.verified = 1;
			}


		}
		i++;
	}
}

int rt_ckpt_mngr::add_vma_to_tree(struct rt_ckpt_driver::vma_dump * l_vma_buf,int verify)
{
	rb_red_blk_node * node = NULL;

	struct vma_save * found_vma = NULL;
	int i = 0;

	while(i < l_vma_buf->return_size)
	{

		node = RBExactQuery(vma_tree,&l_vma_buf->vmas[i].vm_end);
		if(node)
			found_vma = (struct vma_save*)node->key;
		else
			found_vma = NULL;

		if(!found_vma)
		{
			found_vma = (vma_save*)calloc(sizeof(struct vma_save),1);
			found_vma->vm_start = l_vma_buf->vmas[i].vm_start;
			found_vma->vm_end = l_vma_buf->vmas[i].vm_end;
			found_vma->vm_flags = l_vma_buf->vmas[i].vm_flags;
			found_vma->vm_page_prot = l_vma_buf->vmas[i].vm_page_prot;
			found_vma->flags.raw = 0;
			if(!verify)
				found_vma->flags.bits.new_vma = 1;
			if(verify)
				found_vma->flags.bits.new_unverified = 1;
			debug("VMA NEW at 0x%llX-0x%llX flags 0x%llX prot 0x%llX\n",found_vma->vm_start,found_vma->vm_end, found_vma->vm_flags,found_vma->vm_page_prot);
			vma_changes_empty = 0;
			RBTreeInsert(vma_tree, found_vma,found_vma);
		}
		else
		{
			if(verify)
			{
				found_vma->flags.bits.verified = 1;
				found_vma->flags.bits.verified_correct =
						((found_vma->vm_start == l_vma_buf->vmas[i].vm_start) &&
						(found_vma->vm_end == l_vma_buf->vmas[i].vm_end) &&
						(found_vma->vm_flags == l_vma_buf->vmas[i].vm_flags) &&
						(found_vma->vm_page_prot == l_vma_buf->vmas[i].vm_page_prot)) ? 1 : 0;

			}
			if(found_vma->vm_start != l_vma_buf->vmas[i].vm_start)
			{
				debug("VMA MOVE at from 0x%llX to 0x%llX\n",found_vma->vm_start, l_vma_buf->vmas[i].vm_start);
				if(!verify)
				{
					found_vma->vm_start = l_vma_buf->vmas[i].vm_start;
					found_vma->flags.bits.move = 1;
				}
				vma_changes_empty = 0;
			}
			if(found_vma->vm_end < l_vma_buf->vmas[i].vm_end)
			{
				debug("VMA GROW  from 0x%llX to 0x%llX\n",found_vma->vm_end, l_vma_buf->vmas[i].vm_end);
				if(!verify)
				{
					found_vma->vm_end = l_vma_buf->vmas[i].vm_end;
					found_vma->flags.bits.grow = 1;
				}
				vma_changes_empty = 0;
			}
			if(found_vma->vm_end > l_vma_buf->vmas[i].vm_end)
			{
				debug("VMA SHRINK  from 0x%llX to 0x%llX\n",found_vma->vm_end, l_vma_buf->vmas[i].vm_end);
				if(!verify)
				{
					found_vma->vm_end = l_vma_buf->vmas[i].vm_end;
					found_vma->flags.bits.shrink = 1;
				}
				vma_changes_empty = 0;
			}
			if(found_vma->vm_flags != l_vma_buf->vmas[i].vm_flags)
			{
				debug("VMA FLAGS Changed  from 0x%llX to 0x%llX\n",found_vma->vm_flags, l_vma_buf->vmas[i].vm_flags);
				if(!verify)
				{
					found_vma->vm_flags = l_vma_buf->vmas[i].vm_flags;
					found_vma->flags.bits.flags = 1;
				}
				vma_changes_empty = 0;
			}
			if(found_vma->vm_page_prot != l_vma_buf->vmas[i].vm_page_prot)
			{
				debug("VMA PROTECTION Changed  from 0x%llX to 0x%llX\n",found_vma->vm_page_prot, l_vma_buf->vmas[i].vm_page_prot);
				if(!verify)
				{
					found_vma->vm_page_prot = l_vma_buf->vmas[i].vm_page_prot;
					found_vma->flags.bits.prot = 1;
				}
				vma_changes_empty = 0;
			}
		}
		i++;
	}
}
int rt_ckpt_mngr::verify_checkpoint(void)
{
	rb_red_blk_node *  node,*top;
	if(!vma_changes_empty)
	{
		top = node = vma_tree->root->left;
		vma_tree->root->search = 0;
		top->search = 0;

		while(node)
		{
			struct vma_save * found_vma = (struct vma_save*)node->key;
			if(!found_vma)
			{
				node = RBTreeIterate(node,top,vma_tree->nil);
				continue;
			}
			printf("VMA 0x%llX %c%c%c%c%c%c%c%c%c%c\n",found_vma->vm_start,
					found_vma->flags.bits.new_vma ? 'n' : '-',
					found_vma->flags.bits.grow ? 'g' : '-',
					found_vma->flags.bits.shrink ? 's' : '-',
					found_vma->flags.bits.move ? 'm' : '-',
					found_vma->flags.bits.flags ? 'f' : '-',
					found_vma->flags.bits.prot ? 'p' : '-',
					found_vma->flags.bits.clean ? 'c' : '-',
					found_vma->flags.bits.verified ? 'v' : '-',
					found_vma->flags.bits.verified_correct ? '+' : '-',
					found_vma->flags.bits.new_unverified ? 'X' : '-');



			node = RBTreeIterate(node,top,vma_tree->nil);

		}


	}
	//printf("Searching for PTEs ckpt %d \n",checkpoint_id);
	if(!pte_changes_empty)
	{



		top = node = pte_tree->root->left;
		pte_tree->root->search = 0;
		top->search = 0;

		while(node)
		{

			struct pte_save * found_pte = (struct pte_save*)node->key;
			if(!found_pte)
			{
				node = RBTreeIterate(node,top,pte_tree->nil);
				continue;
			}
			//printf("Found PTE 0x%llX\n",found_pte->addr);
			printf("PTE 0x%llX %c%c%c%c%c%c%c%c\n",found_pte->addr,
					found_pte->flags.bits.new_pte ? 'n' : '-',
					found_pte->flags.bits.dirty ? 'd' : '-',
					found_pte->flags.bits.clean ? 'c' : '-',
					found_pte->flags.bits.verified_dirty ? 'D' : '-',
					found_pte->flags.bits.verified_clean ? 'C' : '-',
					found_pte->flags.bits.verified_new ? 'N' : '-',
					found_pte->flags.bits.new_unverified ? 'X' : '-',
					found_pte->flags.bits.verified ? 'v' : '-');




			node = RBTreeIterate(node,top,pte_tree->nil);

		}


	}
}
int rt_ckpt_mngr::write_checkpoint(int reset)
{
	char buf[1024];

	rb_red_blk_node *  node,*top;
	if(!vma_changes_empty)
	{
		sprintf(buf,"%s.%d.vma",this->dump_file_base,this->checkpoint_id);
		FILE *   file = fopen(buf,"wb");
		if(file)
		{
			top = node = vma_tree->root->left;
			vma_tree->root->search = 0;
			top->search = 0;

			while(node)
			{
				struct vma_save * found_vma = (struct vma_save*)node->key;
				if(!found_vma)
				{
					node = RBTreeIterate(node,top,vma_tree->nil);
					continue;
				}
				//printf("Found VMA 0x%llX\n",found_vma->vm_start);

				if(found_vma->flags.raw != 0)
				{
					debug("Writing VMA 0x%llX\n",found_vma->vm_start);
					fwrite(found_vma,sizeof(struct vma_save),1,file);
				}

				if(reset)
					found_vma->flags.raw = 0;

				node = RBTreeIterate(node,top,vma_tree->nil);

			}
			if(reset)
				vma_changes_empty = 1;

			fclose(file);
		}
	}
	//printf("Searching for PTEs ckpt %d \n",checkpoint_id);
	if(!pte_changes_empty)
	{

		sprintf(buf,"%s.%d.pte",this->dump_file_base,this->checkpoint_id);
		FILE *   file = fopen(buf,"wb");
		if(file)
		{
			printf("Writing to %s\n",buf);
			top = node = pte_tree->root->left;
			pte_tree->root->search = 0;
			top->search = 0;

			while(node)
			{

				struct pte_save * found_pte = (struct pte_save*)node->key;
				if(!found_pte)
				{
					node = RBTreeIterate(node,top,pte_tree->nil);
					continue;
				}
				//printf("Found PTE 0x%llX\n",found_pte->addr);
				if(found_pte->flags.raw != 0)
				{
					debug("Writing PTE 0x%llX\n",found_pte->addr);
					fwrite(found_pte,sizeof(struct pte_save),1,file);
				}

				if(reset)
				{
					found_pte->dirty_bits = 0;
					found_pte->flags.raw = 0;

				}

				node = RBTreeIterate(node,top,pte_tree->nil);

			}
			if(reset)
				pte_changes_empty = 1;

			fclose(file);
		}
	}
}

int rt_ckpt_mngr::replay_checkpoint(int pid, int reset)
{
	rb_red_blk_node *  node,*top;
	struct rt_ckpt_driver::vma_desc_message vma_desc_message;
	struct rt_ckpt_driver::pte_write_message  pte_write_message;
	if(!vma_changes_empty)
	{
		top = node = vma_tree->root->left;
		vma_tree->root->search = 0;
		top->search = 0;

		while(node)
		{
			struct vma_save * found_vma = (struct vma_save*)node->key;
			if(!found_vma)
			{
				node = RBTreeIterate(node,top,vma_tree->nil);
				continue;
			}
			//printf("Found VMA 0x%llX\n",found_vma->vm_start);

			//if(found_vma->flags.raw != 0)
			//{
				//fwrite(found_vma,sizeof(struct vma_save),1,file);
				vma_desc_message.vm_start = found_vma->vm_start;
				vma_desc_message.vm_end = found_vma->vm_end;
				vma_desc_message.vm_flags = found_vma->vm_flags;
				vma_desc_message.vm_page_prot = found_vma->vm_page_prot;
				vma_desc_message.pid = pid;
				driver.ioctl_write_vma_msg(&vma_desc_message);
			//}

			if(reset)
				found_vma->flags.raw = 0;

			node = RBTreeIterate(node,top,vma_tree->nil);

		}
		if(reset)
			vma_changes_empty = 1;

	}
	//printf("Searching for PTEs ckpt %d \n",checkpoint_id);
	if(!pte_changes_empty)
	{

		top = node = pte_tree->root->left;
		pte_tree->root->search = 0;
		top->search = 0;

		while(node)
		{

			struct pte_save * found_pte = (struct pte_save*)node->key;
			if(!found_pte)
			{
				node = RBTreeIterate(node,top,pte_tree->nil);
				continue;
			}
			//printf("Found PTE 0x%llX\n",found_pte->addr);
			//if(found_pte->flags.raw != 0)
			//{
				//printf("Writing PTE 0x%llX\n",found_pte->addr);
				pte_write_message.addr = found_pte->addr;
				pte_write_message.pid = pid;
				memcpy(pte_write_message.data,found_pte->data,RT_CKPT_PAGE_SIZE);
				driver.ioctl_write_pte_msg(&pte_write_message);
			//}

			if(reset)
			{
				found_pte->dirty_bits = 0;
				found_pte->flags.raw = 0;

			}

			node = RBTreeIterate(node,top,pte_tree->nil);

		}
		if(reset)
			pte_changes_empty = 1;

	}
}



int rt_ckpt_mngr::monitor(pid_t pid)
{
	char buf[1024];
	sprintf(buf,"%s.vma",dump_file_base);
	init_dump_file(buf);
	driver.ioctl_monitor_msg(pid,buf);
}

int rt_ckpt_mngr::pop(pid_t pid)
{
	driver.ioctl_pop_msg(pid,ckpt_buf,SIZE_CPKT_BUF,0);
}

int rt_ckpt_mngr::concurrent_overhead(pid_t pid,int max_pop_size)
{
	signal(SIGINT, INThandler);
	int vma_dump_size = 1024;
	unsigned long start,vma_time,reg_time,pop_time,last_ckpt_time;
	struct rt_ckpt_driver::vma_dump * vma_dump = (struct rt_ckpt_driver::vma_dump *)malloc(sizeof(struct rt_ckpt_driver::vma_dump) + sizeof(struct rt_ckpt_driver::vma_desc_message) * vma_dump_size);
	vma_dump->size = vma_dump_size;
	vma_dump->pid = pid;
	rb_red_blk_node * node = NULL;
	char buf[1024];
	FILE * file = NULL;
	int total_popped = 0;

	driver.ioctl_pid_msg(pid);
	sprintf(buf,"%s.pre_dump",dump_file_base);
	init_dump_file(buf);

	struct rt_ckpt_driver::checkpoint_msg* pvt_ckpt_buf = (struct rt_ckpt_driver::checkpoint_msg*)malloc(sizeof(struct rt_ckpt_driver::checkpoint_msg)*max_pop_size);
	int numtid = gettids(pid,tid_buf);
	struct user_regs_struct regs;
	struct user_fpregs_struct fpregs;
	int status;

	printf("num tids %d\n",numtid);
	for (int i = 0; i < numtid; i++)
	{
		printf("attaching to PID %d\n",tid_buf[i]);
	  ptrace(PTRACE_ATTACH, tid_buf[i],
					 NULL, NULL);
	  waitpid(tid_buf[i],&status,0);

	}
	driver.ioctl_checkpoint_stall_msg(pid,buf);
	sprintf(buf,"%s.regs",dump_file_base);
	file = fopen(buf,"w");
	if(file)
	{
	  fwrite(&numtid,sizeof(int),1,file);
	  for (int i = 0; i < numtid; i++)
		  fwrite(&tid_buf[i],sizeof(int),1,file);
	  for (int i = 0; i < numtid; i++)
	  {

		  ptrace(PTRACE_GETREGS, tid_buf[i],
				 NULL, &regs);
		  ptrace(PTRACE_GETFPXREGS, tid_buf[i],
						 NULL, &fpregs);;

		  fwrite(&regs,sizeof(struct user_regs_struct),1,file);
		  fwrite(&fpregs,sizeof(struct user_fpregs_struct),1,file);

	  }
	  fclose(file);

	}
	else
	{
	  printf("Failed to open %s",buf);
	}

	for (int i = 0; i < numtid; i++)
	{
	ptrace(PTRACE_DETACH, tid_buf[i],
						NULL, NULL);
	}
	printf("Running concurrent checkpoint\n");
	int sleep_count = 0;
	last_ckpt_time = _tsc_read();
	while(keepRunning && driver.error == 0)
	{

		start = _tsc_read();
		int return_size = 0;driver.ioctl_pop_msg(pid,pvt_ckpt_buf,max_pop_size,0);
		pop_time =_tsc_read() - start;
//		if(return_size > 0)
//			debug("pop time = %d per page and %d pages\n",pop_time/return_size,return_size);
		total_popped += return_size;
		if(total_popped < max_pop_size)
		{
			usleep(checkpoint_period/10);
		}
		event_log(pid);
		start = _tsc_read();
	}
	printf("Exiting concurrent Checkpoint\n");

}

int rt_ckpt_mngr::concurrent(pid_t pid,int max_pop_size)
{
	signal(SIGINT, INThandler);
	int vma_dump_size = 1024;
	unsigned long start,vma_time,reg_time,pop_time,last_ckpt_time;
	struct rt_ckpt_driver::vma_dump * vma_dump = (struct rt_ckpt_driver::vma_dump *)malloc(sizeof(struct rt_ckpt_driver::vma_dump) + sizeof(struct rt_ckpt_driver::vma_desc_message) * vma_dump_size);
	vma_dump->size = vma_dump_size;
	vma_dump->pid = pid;
	rb_red_blk_node * node = NULL;
	char buf[1024];
	FILE * file = NULL;
	int total_popped = 0;

	driver.ioctl_pid_msg(pid);
	sprintf(buf,"%s.pre_dump",dump_file_base);
	init_dump_file(buf);

	struct rt_ckpt_driver::checkpoint_msg* pvt_ckpt_buf = (struct rt_ckpt_driver::checkpoint_msg*)malloc(sizeof(struct rt_ckpt_driver::checkpoint_msg)*max_pop_size);
	int numtid = gettids(pid,tid_buf);
	struct user_regs_struct regs;
	struct user_fpregs_struct fpregs;
	int status;

	printf("num tids %d\n",numtid);
	for (int i = 0; i < numtid; i++)
	{
		printf("attaching to PID %d\n",tid_buf[i]);
	  ptrace(PTRACE_ATTACH, tid_buf[i],
					 NULL, NULL);
	  waitpid(tid_buf[i],&status,0);

	}
	driver.ioctl_checkpoint_stall_msg(pid,buf);
	sprintf(buf,"%s.regs",dump_file_base);
	file = fopen(buf,"w");
	if(file)
	{
	  fwrite(&numtid,sizeof(int),1,file);
	  for (int i = 0; i < numtid; i++)
		  fwrite(&tid_buf[i],sizeof(int),1,file);
	  for (int i = 0; i < numtid; i++)
	  {

		  ptrace(PTRACE_GETREGS, tid_buf[i],
				 NULL, &regs);
		  ptrace(PTRACE_GETFPXREGS, tid_buf[i],
						 NULL, &fpregs);;

		  fwrite(&regs,sizeof(struct user_regs_struct),1,file);
		  fwrite(&fpregs,sizeof(struct user_fpregs_struct),1,file);

	  }
	  fclose(file);

	}
	else
	{
	  printf("Failed to open %s",buf);
	}

	for (int i = 0; i < numtid; i++)
	{
	ptrace(PTRACE_DETACH, tid_buf[i],
						NULL, NULL);
	}
	printf("Running concurrent checkpoint\n");
	int sleep_count = 0;
	last_ckpt_time = _tsc_read();
	while(keepRunning && driver.error == 0)
	{

		start = _tsc_read();
		int return_size = driver.ioctl_pop_msg(pid,pvt_ckpt_buf+total_popped,max_pop_size-total_popped,0);
		pop_time =_tsc_read() - start;
//		if(return_size > 0)
//			debug("pop time = %d per page and %d pages\n",pop_time/return_size,return_size);
		total_popped += return_size;
		if(total_popped < max_pop_size)
		{
			usleep(checkpoint_period/10);
		}



		if(start - last_ckpt_time > (checkpoint_period*CKPT_CPU_FREQ))
		{
			sleep_count = 0;
			debug("Setting ckpt_inprogress\n");
			//for chekcing purposes
			if(checkpoint_verify)
			{
				debug("Halting all threads with PTRACE\n");
				for (int i = 0; i < numtid; i++)
				{

				  ptrace(PTRACE_ATTACH, tid_buf[i],
								 NULL, NULL);
				  waitpid(tid_buf[i],&status,0);
				}
			}
			return_size = 1;
			int retry = 3;
			last_ckpt_time = _tsc_read();
			while(return_size && driver.error == 0 && retry > 0)
			{
				return_size = driver.ioctl_pop_msg(pid,pvt_ckpt_buf+total_popped,max_pop_size-total_popped,1);
				total_popped += return_size;
			}
			//usleep(10000000);
			//driver.ioctl_get_regs_msg(numtid,tid_buf);
			//dump vma_list just incase of changes

			driver.ioctl_vma_dump_msg(vma_dump);
			debug("Dumped VMA with size %d\n",vma_dump->return_size);
			while(vma_dump->return_size == vma_dump->size && driver.error == 0)
			{
				free(vma_dump);
				vma_dump_size *= 2;
				vma_dump = (struct rt_ckpt_driver::vma_dump *)malloc(sizeof(struct rt_ckpt_driver::vma_dump) + sizeof(struct rt_ckpt_driver::vma_desc_message) * vma_dump_size);
				vma_dump->size = vma_dump_size;
				vma_dump->pid = pid;
				driver.ioctl_vma_dump_msg(vma_dump);
				debug("Resized buffer and Dumped VMA with size %d\n",vma_dump->return_size);

			}
			//for chekcing purposes
			if(checkpoint_verify)
			{


				sprintf(buf,"%s.%d.verify_dump",dump_file_base,checkpoint_id);
				init_dump_file(buf);
				driver.ioctl_checkpoint_msg(pid,buf);
			}
			numtid = gettids(pid,tid_buf);
			struct rt_ckpt_driver::pid_list pid_list;
			driver.ioctl_get_regs_msg(numtid,tid_buf,&pid_list);
			if(checkpoint_verify)
			{
				debug("Resuming all threads with PTRACE\n");
				for (int i = 0; i < numtid; i++)
				{
				ptrace(PTRACE_DETACH, tid_buf[i],
									NULL, NULL);
				}
			}

			sprintf(buf,"%s.%d.regs",dump_file_base,checkpoint_id);
			file = fopen(buf,"w");
			fwrite(&numtid,sizeof(int),1,file);
			for (int i = 0; i < numtid; i++)
				fwrite(&tid_buf[i],sizeof(int),1,file);
			for(int i =0; i <  numtid; i++)
			{
				//print_regs(&pid_list.user_regs_struct[i]);
				fwrite(&pid_list.user_regs_struct[i],sizeof(struct user_regs_struct),1,file);
				fwrite(&pid_list.user_fpregs_struct[i],sizeof(struct user_fpregs_struct),1,file);
			}
			fclose(file);

			debug("Last pop Read\n");
			return_size = driver.ioctl_pop_msg(pid,pvt_ckpt_buf,max_pop_size-total_popped,1);
			total_popped += return_size;

			debug("Checking all dirty ptes for changes\n");
			int old_total = total_popped;
			for(int i = 0; i < old_total; i++)
			{
				debug("Read PTE at 0x%llX\n",pvt_ckpt_buf[i].vma_address);
				struct rt_ckpt_driver::pte_write_message pte_msg;
				pte_msg.pid = pid;
				pte_msg.addr = pvt_ckpt_buf[i].vma_address;
				pte_msg.remapped_addr = pvt_ckpt_buf[i].remapped_addr;
				pvt_ckpt_buf[total_popped].vma_address = pte_msg.addr;
				driver.ioctl_read_pte(&pte_msg);
				memcpy(pvt_ckpt_buf[total_popped].data,pte_msg.data,4096);
				total_popped++;


			}




			debug("Clearing ckpt_inprogress\n");
			return_size = driver.ioctl_pop_msg(pid,pvt_ckpt_buf,max_pop_size-total_popped,0);
			total_popped += return_size;

			add_pte_to_tree(pvt_ckpt_buf,total_popped);
			add_vma_to_tree(vma_dump);
			verify_checkpoint();
			write_checkpoint(true);
			event_log(pid);


			total_popped = 0;
			vma_dump->return_size = 0;
			checkpoint_id ++;

		}
		start = _tsc_read();
	}
	printf("Exiting concurrent Checkpoint\n");
}

int rt_ckpt_mngr::event_log(pid_t pid)
{

	pid_log.pid = pid;
	driver.ioctl_get_log(&pid_log);
	if(!first_start)
		first_start = pid_log.buf[0].rdtsc;
	for(int i = 0; i < pid_log.return_size; i ++)
	{
		struct rt_ckpt_driver::checkpoint_event_queue_entry* qe = &pid_log.buf[i];
		printf("%s CPU %d of PID %d arg1 0x%X arg2 0x%X at 0x%llX\n",
				checkpoint_event_type_strings[qe->type],
				qe->cpu,qe->pid,qe->arg1,qe->arg2,(unsigned long)((qe->rdtsc-first_start)/(CKPT_CPU_FREQ/10.0)));
	}
}
int rt_ckpt_mngr::checkpoint(pid_t pid)
{
	unsigned long start,vma_time,reg_time,pop_time;
	char buf[1024];
	FILE * file = NULL;

	start = _tsc_read();
	driver.ioctl_pid_msg(pid);
	int numtid = gettids(pid,tid_buf);

	sprintf(buf,"%s.vma",dump_file_base);
	init_dump_file(buf);


	struct user_regs_struct regs;
	struct user_fpregs_struct fpregs;
	int status;

	printf("num tids %d\n",numtid);
	for (int i = 0; i < numtid; i++)
	{

	  ptrace(PTRACE_ATTACH, tid_buf[i],
					 NULL, NULL);
	  waitpid(tid_buf[i],&status,0);

	}

	driver.ioctl_checkpoint_msg(pid,buf);
	vma_time = _tsc_read() - start;



	sprintf(buf,"%s.regs",dump_file_base);
	file = fopen(buf,"w");
	if(file)
	{
	  fwrite(&numtid,sizeof(int),1,file);
	  for (int i = 0; i < numtid; i++)
		  fwrite(&tid_buf[i],sizeof(int),1,file);
	  for (int i = 0; i < numtid; i++)
	  {

		  ptrace(PTRACE_GETREGS, tid_buf[i],
				 NULL, &regs);
		  ptrace(PTRACE_GETFPXREGS, tid_buf[i],
						 NULL, &fpregs);

		  fwrite(&regs,sizeof(struct user_regs_struct),1,file);
		  fwrite(&fpregs,sizeof(struct user_fpregs_struct),1,file);

	  }
	  fclose(file);

	}
	else
	{
	  printf("Failed to open %s",buf);
	}

	for (int i = 0; i < numtid; i++)
	{
		ptrace(PTRACE_DETACH, tid_buf[i],
						NULL, NULL);
	}

	reg_time =_tsc_read() - vma_time;
	printf("VMA Dump Time %u\nREG Dump Time %u\n",vma_time,reg_time);
}

int rt_ckpt_mngr::replay_regs()
{
	printf("Replay\n");
	int status;
	char buf[1024];
	FILE * file = NULL;
	struct rt_ckpt_driver::pid_list pid_list;
	sprintf(buf,"%s.regs",dump_file_base);
	file = fopen(buf,"r");
	fread(&numtid,sizeof(int),1,file);
	for (int i = 0; i < numtid; i++)
		fread(&tid_buf[i],sizeof(int),1,file);


	for (int i = 0; i < numtid; i++)
	{
		ptrace(PTRACE_ATTACH, tid_buf[i],
						 NULL, NULL);
		waitpid(tid_buf[i],&status,0);
	}

	for (int i = 0; i < numtid; i++)
	{

		printf("replaying tid %d\n",tid_buf[i]);


		struct user_regs_struct regs;
		struct user_fpregs_struct fpregs;
		fread(&regs,sizeof(struct user_regs_struct),1,file);
		fread(&fpregs,sizeof(struct user_fpregs_struct),1,file);
		ptrace(PTRACE_SETREGS, tid_buf[i],
			 NULL, &regs);
		ptrace(PTRACE_SETFPXREGS, tid_buf[i],
					 NULL, &fpregs);
		unsigned long ins = ptrace(PTRACE_PEEKTEXT, tid_buf[i],
				   regs.rip, NULL);
		printf("EIP: %lx Instruction executed: %lx\n",
			 regs.rip, ins);


	}
	fclose(file);

	return tid_buf[0];



}
int rt_ckpt_mngr::start_threads()
{
	for (int i = 0; i < numtid; i++)
	{
		ptrace(PTRACE_DETACH, tid_buf[i],
					 NULL, NULL);
	}
}

void rt_ckpt_mngr::print_regs(struct user_regs_struct *regs)
{
	printf(" r15 0x%llX\n",regs->r15);
	printf(" r14 0x%llX\n",regs->r14);
	printf(" r13 0x%llX\n",regs->r13);
	printf(" r12 0x%llX\n",regs->r12);
	printf(" rbp 0x%llX\n",regs->rbp);
	printf(" rbx 0x%llX\n",regs->rbx);
	printf(" r11 0x%llX\n",regs->r11);
	printf(" r10 0x%llX\n",regs->r10);
	printf(" r9 0x%llX\n",regs->r9);
	printf(" r8 0x%llX\n",regs->r8);
	printf(" rax 0x%llX\n",regs->rax);
	printf(" rcx 0x%llX\n",regs->rcx);
	printf(" rdx 0x%llX\n",regs->rdx);
	printf(" rsi 0x%llX\n",regs->rsi);
	printf(" rdi 0x%llX\n",regs->rdi);
	printf(" orig_rax 0x%llX\n",regs->orig_rax);
	printf(" rip 0x%llX\n",regs->rip);
	printf(" cs 0x%llX\n",regs->cs);
	printf(" eflags 0x%llX\n",regs->eflags);
	printf(" rsp 0x%llX\n",regs->rsp);
	printf(" ss 0x%llX\n",regs->ss);
	printf(" fs_base 0x%llX\n",regs->fs);
	printf(" gs_base 0x%llX\n",regs->gs);
	printf(" ds 0x%llX\n",regs->ds);
	printf(" es 0x%llX\n",regs->es);
	printf(" fs 0x%llX\n",regs->fs);
	printf(" gs 0x%llX\n",regs->gs);
}

int rt_ckpt_mngr::ptrace_debug(pid_t pid)
{
	printf("Ptrace\n");

	struct user_regs_struct regs;
	struct user_fpregs_struct fpregs;

	ptrace(PTRACE_ATTACH, pid,
		 NULL, NULL);
	wait(NULL);
	ptrace(PTRACE_GETREGS, pid,
		 NULL, &regs);
	ptrace(PTRACE_GETFPXREGS, pid,
				 NULL, &fpregs);
	unsigned long ins = ptrace(PTRACE_PEEKTEXT, pid,
			   regs.rip, NULL);
	printf("EIP: %lx Instruction executed: %lx\n",
		 regs.rip, ins);
	ptrace(PTRACE_DETACH, pid,
		 NULL, NULL);

	printf(" r15 0x%llX\n",regs.r15);
	printf(" r14 0x%llX\n",regs.r14);
	printf(" r13 0x%llX\n",regs.r13);
	printf(" r12 0x%llX\n",regs.r12);
	printf(" rbp 0x%llX\n",regs.rbp);
	printf(" rbx 0x%llX\n",regs.rbx);
	printf(" r11 0x%llX\n",regs.r11);
	printf(" r10 0x%llX\n",regs.r10);
	printf(" r9 0x%llX\n",regs.r9);
	printf(" r8 0x%llX\n",regs.r8);
	printf(" rax 0x%llX\n",regs.rax);
	printf(" rcx 0x%llX\n",regs.rcx);
	printf(" rdx 0x%llX\n",regs.rdx);
	printf(" rsi 0x%llX\n",regs.rsi);
	printf(" rdi 0x%llX\n",regs.rdi);
	printf(" orig_rax 0x%llX\n",regs.orig_rax);
	printf(" rip 0x%llX\n",regs.rip);
	printf(" cs 0x%llX\n",regs.cs);
	printf(" eflags 0x%llX\n",regs.eflags);
	printf(" rsp 0x%llX\n",regs.rsp);
	printf(" ss 0x%llX\n",regs.ss);
	printf(" fs_base 0x%llX\n",regs.fs);
	printf(" gs_base 0x%llX\n",regs.gs);
	printf(" ds 0x%llX\n",regs.ds);
	printf(" es 0x%llX\n",regs.es);
	printf(" fs 0x%llX\n",regs.fs);
	printf(" gs 0x%llX\n",regs.gs);

}

/* Main - Call the ioctl functions */
int main(int argc, char * argv[])
{
	int file_desc, ret_val;
	unsigned long start,vma_time,reg_time,pop_time;
	int tid_buf[1024];
	memset(tid_buf,0,sizeof(int)*1024);
	int numtid = 0;
	int begin = 0;
	int end = 0;

	char buf[1024];
	FILE * file = NULL;
	memset(buf,0,1024);

	rt_ckpt_mngr rt_ckpt_mngr;

	char* mode, *output_file,*ckpt_file;
	pid_t pid;
	unsigned int pop_size;


	mode = "help";
	output_file = NULL;
	ckpt_file = NULL;
	pid = 0;
	pop_size = RT_CKPT_PAGE_SIZE;
	int c ;
	int ckpt_freq = 0;
	while( ( c = getopt (argc, argv, "p:o:m:s:h:b:e:c:f:") ) != -1 )
	{
		switch(c)
		{
			case 'm':
				if(optarg) mode = optarg;
				break;
			case 'o':
				if(optarg) output_file = optarg ;
				break;
			case 'c':
				if(optarg) ckpt_file = optarg ;
				break;
			case 'p':
				if(optarg) pid = std::atoi(optarg) ;
				break;
			case 's':
				if(optarg) pop_size = std::atoi(optarg) ;
				break;
			case 'h':
				mode = "help";
				break;
			case 'b':
				if(optarg) begin = std::atoi(optarg) ;
				break;
			case 'e':
				if(optarg) end = std::atoi(optarg) ;
				break;
			case 'f':
				if(optarg) ckpt_freq = std::atoi(optarg) ;
				break;
			}
	}
	rt_ckpt_mngr.checkpoint_period = ckpt_freq > 0 ? 1000000/ckpt_freq : CKPT_TIME_THRESHOLD_US;
	std::cerr << "Checkpoint period is " << rt_ckpt_mngr.checkpoint_period << std::endl;
	if(argc > 2)
	{
		if(!strcmp(mode,"syscall"))
		{
			syscall(333,25);
		}
		if(!strcmp(mode,"pid"))
		{
			rt_ckpt_mngr.driver.ioctl_pid_msg(pid);
		}
		else if(!strcmp(mode,"pop"))
		{

		}
		else if(!strcmp(mode,"concurrent_overhead"))
		{

			rt_ckpt_mngr.dump_file_base = output_file;
			rt_ckpt_mngr.concurrent_overhead(pid,pop_size);
		}
		else if(!strcmp(mode,"concurrent"))
		{

			rt_ckpt_mngr.dump_file_base = output_file;
			rt_ckpt_mngr.concurrent(pid,pop_size);
		}
		else if(!strcmp(mode,"concurrent_verify"))
		{
			rt_ckpt_mngr.checkpoint_verify = 1;
			rt_ckpt_mngr.dump_file_base = output_file;
			rt_ckpt_mngr.concurrent(pid,pop_size);

		}
		else if(!strcmp(mode,"checkpoint"))
		{



		}
		else if(!strcmp(mode,"replay_file"))
		{
			rt_ckpt_mngr.dump_file_base = output_file;
			pid = rt_ckpt_mngr.replay_regs();
			rt_ckpt_mngr.read_base_checkpoint_file(output_file,ckpt_file);
			rt_ckpt_mngr.replay_checkpoint(pid,true);
			rt_ckpt_mngr.start_threads();


		}
		else if(!strcmp(mode,"ptrace"))
		{

		}
		else if(!strcmp(mode,"squash"))
		{
			rt_ckpt_mngr.dump_file_base = output_file;
			rt_ckpt_mngr.squash_checkpoints(begin,end);
		}
		else if(!strcmp(mode,"squash_verify"))
		{
			rt_ckpt_mngr.checkpoint_verify = 1;
			rt_ckpt_mngr.dump_file_base = output_file;
			rt_ckpt_mngr.squash_checkpoints(begin,end);
		}
		else if(!strcmp(mode,"help"))
		{
			std::cout << std::endl;
			std::cout << "Real-Time Checkpoint Manager " << std::endl;
			std::wcout << "Copyright" << (wchar_t)0xA9 << "Michael Prinke  2018" << std::endl;
			std::cout << std::endl;
			std::cout << "Usage :" <<std::endl;
			std::cout << "\trt_ckpt -m <mode> -p <pid> -o <output_base_filepath> [-s <pop_size>]" <<std::endl;
			std::cout << "Modes:" << std::endl;
			std::cout << "\tconcurrent:" << std::endl;
			std::cout << "\tpop:" << std::endl;
			std::cout << "\tcheckpoint:" << std::endl;
			std::cout << "\treplay_file:" << std::endl;
			std::cout << "\tptrace:" << std::endl;
			std::cout << "\thelp:" << std::endl;
		}


	}

  //ioctl_get_msg(file_desc);
  //ioctl_set_msg(file_desc, msg);

  //close(file_desc);

  //RBTreeDestroy(vma_tree);
}


