/*
 * rt_ckpt_mngr.h
 *
 *  Created on: Jun 17, 2018
 *      Author: mprinke
 */

#ifndef RT_CKPT_MNGR_H_
#define RT_CKPT_MNGR_H_

#include "rt_ckpt_driver.h"
#include "red_black_tree.h"
#define CKPT_CPU_FREQ 1916
#define CKPT_TIME_THRESHOLD_US 10000
#define CKPT_TIME_THRESHOLD_TSC (CKPT_CPU_FREQ*CKPT_TIME_THRESHOLD_US)
#define DEFUALT_CKPT_EVENT_QUEUE_SIZE (1024*1024)
class rt_ckpt_mngr {
public:
	rt_ckpt_mngr();
	virtual ~rt_ckpt_mngr();
	static int keepRunning;
	rt_ckpt_driver driver;

	int monitor(pid_t pid);
	int pop(pid_t pid);
	int concurrent(pid_t pid,int max_pop_size);
	int concurrent_overhead(pid_t pid,int max_pop_size);
	int checkpoint(pid_t pid);
	int replay(pid_t pid,int num_files, char * file_paths);
	int ptrace_debug(pid_t pid);
	int squash_checkpoints(int start, int stop);
	int read_base_checkpoint_file(char * base_file,char * ckpt_file);
	int replay_checkpoint(int pid,int reset);
	int replay_regs();
	int start_threads();
	int verify_checkpoint();
	int load_dump_file(char * filename);
	char * dump_file_base;
	int checkpoint_verify;
	int event_log(pid_t pid);
	int checkpoint_period;

private:
	struct rt_ckpt_driver::pid_log pid_log;
	rb_red_blk_tree *vma_tree;
	rb_red_blk_tree *pte_tree;
	#define SIZE_CPKT_BUF 128
	struct rt_ckpt_driver::checkpoint_msg ckpt_buf[SIZE_CPKT_BUF];
	int numtid = 0;
	int tid_buf[1024];
	long checkpoint_id;
	unsigned long first_start = 0;

	struct pte_save
	{
		unsigned long long addr;
		unsigned long long dirty_bits;
		union
		{
			struct
			{
			  int new_pte  : 1;
			  int dirty : 1;
			  int clean : 1;
			  int verified_dirty : 1;
			  int verified_clean : 1;
			  int verified_new : 1;
			  int new_unverified : 1;
			  int verified : 1;

			} bits;
			int raw;
		} flags;

		char data[4096];
	};

	struct vma_save
	{
		unsigned long vm_end;
		unsigned long vm_start;
		unsigned long vm_flags;
		unsigned long vm_page_prot;
		union
		{
			struct
			{
			  int new_vma : 1 ;
			  int grow  : 1;
			  int shrink : 1;
			  int move : 1;
			  int flags : 1;
			  int prot : 1;
			  int clean : 1;
			  int verified : 1;
			  int verified_correct : 1;
			  int new_unverified : 1;

			} bits;
			int raw;
		} flags;
		//unsigned long addr;
		//unsigned long end;

	};

	int pte_changes_empty;
	int vma_changes_empty;

	static int
	cmp_vma_save (const void * a,const void * b);
	static int
	cmp_pte_save (const void * a, const void * b);

	int add_pte_to_tree(struct rt_ckpt_driver::checkpoint_msg * l_ckpt_buf, int num_entries,int verify = false);
	int add_vma_to_tree(struct rt_ckpt_driver::vma_dump * l_vma_buf,int verify = false);

	int write_checkpoint(int reset);


	int gettids(int pid, int * tid_buf);

	int init_dump_file(char * file_path);
	void print_regs(struct user_regs_struct *regs);

	enum ckpt_commands {
		CKPT_SYSCALL_STALL = 1,
		CKPT_SYSCALL_END_FRAME,
	};

};

#endif /* RT_CKPT_MNGR_H_ */
