/*
 * rtckptdriver.h
 *
 *  Created on: Jun 17, 2018
 *      Author: mprinke
 */

#ifndef RT_CKPT_DRIVER_H_
#define RT_CKPT_DRIVER_H_


#include <sys/user.h>
#include <fcntl.h>      /* open */
#include <sys/ioctl.h>  /* ioctl */
#include <sys/types.h>

#define RT_CKPT_IOCTL_PID_MSG _IOR(100, 0, char *)
#define RT_CKPT_IOCTL_MONITOR_MSG _IOR(100, 1, char *)
#define RT_CKPT_IOCTL_CHECKPOINT_MSG _IOR(100, 2, char *)
#define RT_CKPT_IOCTL_REPLAY_MSG _IOR(100, 3, char *)
#define RT_CKPT_IOCTL_WRITE_VMA_MSG _IOR(100, 4, char *)
#define RT_CKPT_IOCTL_WRITE_PTE_MSG _IOR(100, 5, char *)
#define RT_CKPT_IOCTL_POP_QUEUE _IOR(100, 6, char *)
#define RT_CKPT_IOCTL_GET_REGS _IOR(100, 7, char *)
#define RT_CKPT_IOCTL_VMA_DUMP_MSG _IOR(100, 8, char *)
#define RT_CKPT_IOCTL_CHECKPOINT_STALL_MSG _IOR(100, 9, char *)
#define RT_CKPT_IOCTL_READ_PTE_MSG _IOR(100, 10, char *)
#define RT_CKPT_IOCTL_GET_LOG _IOR(100, 11, char *)

#define RT_CKPT_SIZE_CPKT_BUF 128
#define RT_CKPT_PAGE_SIZE 4096
class rt_ckpt_driver {
public:
	rt_ckpt_driver();
	virtual ~rt_ckpt_driver();

	struct checkpoint_msg
	{
		unsigned long long vma_address;
		unsigned long long remapped_addr;
		char data[RT_CKPT_PAGE_SIZE];
	};
	struct pte_write_message
	{
		int pid;
		unsigned long long addr;
		unsigned long long remapped_addr;
		unsigned char data[RT_CKPT_PAGE_SIZE];
	};


	struct vma_desc_message
	{
		int pid;
		unsigned long vm_end;
		unsigned long vm_start;
		unsigned long vm_flags;
		unsigned long vm_page_prot;

		//unsigned long addr;
		//unsigned long end;
	};

	struct vma_dump
	{
		int pid;
		int size;
		int return_size;
		struct vma_desc_message vmas[];

	};
	struct pid_list
	{
		int size;
		int pids[16];
		struct user_regs_struct user_regs_struct[16];
		struct user_fpregs_struct user_fpregs_struct[16];
		int pid_status[16];
	};
	enum checkpoint_event_type
	{
		CKPT_EVENT_UNKNOWN = 0,
		CKPT_EVENT_PAGE_FAULT_ENTRY,
		CKPT_EVENT_PAGE_FAULT_ANON,
		CKPT_EVENT_PAGE_FAULT_DIRTY,
		CKPT_EVENT_PAGE_FAULT_FINISH,
		CKPT_EVENT_PAGE_FAULT_STALL,
		CKPT_EVENT_PAGE_FAULT_RESUME,

		CKPT_EVENT_SWAP_IN,
		CKPT_EVENT_SWAP_OUT,

		CKPT_EVENT_SMP_FUNC_CALL_ISSUE,
		CKPT_EVENT_SMP_FUNC_CALL_START,
		CKPT_EVENT_SMP_FUNC_CALL_FINISH,
		CKPT_EVENT_SMP_FUNC_CALL_COMPLETE,

		CKPT_EVENT_POP_PAGE,
		CKPT_EVENT_PAGE_WALK_START,
		CKPT_EVENT_PAGE_WALK_COMPLETE,
		CKPT_EVENT_MEMREMAP_START,
		CKPT_EVENT_MEMREMAP_COMPLETE,
		CKPT_EVENT_PAGE_COPY_START,
		CKPT_EVENT_PAGE_COPY_COMPLETE,
		CKPT_EVENT_CLEAR_DIRTY,

		CKPT_EVENT_CHECKPOINT_START,
		CKPT_EVENT_CHECKPOINT_COMPLETE,
		CKPT_EVENT_THREAD_HALTED,
		CKPT_EVENT_TRHEAD_UNQUEUED


	} ;
	struct checkpoint_event_queue_entry
	{

		unsigned long rdtsc;
		pid_t pid;
		int cpu;
		unsigned long arg1;
		unsigned long arg2;
		enum checkpoint_event_type type;

	};

	struct pid_log
	{
		int pid;
		int size;
		int return_size;
		struct checkpoint_event_queue_entry * buf;
	};

	void ioctl_pid_msg(pid_t pid);
	void ioctl_get_regs_msg(int num_pids,pid_t * pid,struct pid_list *pid_list);
	int ioctl_pop_msg(pid_t pid,struct checkpoint_msg * buf,int size_buf,int set_ckpt_inprogress);
	void ioctl_monitor_msg(pid_t pid, char *message);
	void ioctl_replay_msg(char *message);
	void ioctl_checkpoint_msg(pid_t pid, char *message);
	void ioctl_checkpoint_stall_msg(pid_t pid, char *message);
	void ioctl_write_pte_msg(struct pte_write_message *message);
	void ioctl_write_vma_msg(struct vma_desc_message *message);
	void ioctl_vma_dump_msg(struct vma_dump *message);
	void ioctl_read_pte(struct pte_write_message *pte_write_message);
	void ioctl_get_log(struct pid_log *pid_log);
	int error;
private:
	int file_desc;




	struct pid_only
	{
		int pid;
	};

	struct pid_pop
	{
		int pid;
		int size;
		int return_size;
		int set_ckpt_inprogress;
		struct checkpoint_msg * buf;

	};
	struct pid_file_message
	{
		int pid;
		char file_path[1024];
	};



	char *  dev_file_name  = "/dev/creplay";
	struct checkpoint_msg ckpt_buf[RT_CKPT_SIZE_CPKT_BUF];


};

#endif /* RT_CKPT_DRIVER_H_ */
