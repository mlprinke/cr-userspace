
#include <fcntl.h>      /* open */
#include <unistd.h>     /* exit */
#include <sys/ioctl.h>  /* ioctl */
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <linux/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include "red_black_tree.h"
#include <sys/types.h>
#include <dirent.h>
#include <include/asm/ptrace.h>
#include <cstring>

using namespace std;

#define DEVICE_FILE_NAME "/dev/creplay"


#define IOCTL_PID_MSG _IOR(100, 0, char *)
#define IOCTL_MONITOR_MSG _IOR(100, 1, char *)
#define IOCTL_CHECKPOINT_MSG _IOR(100, 2, char *)
#define IOCTL_REPLAY_MSG _IOR(100, 3, char *)
#define IOCTL_WRITE_VMA_MSG _IOR(100, 4, char *)
#define IOCTL_WRITE_PTE_MSG _IOR(100, 5, char *)

#define IOCTL_POP_QUEUE _IOR(100, 6, char *)
#define IOCTL_GET_REGS _IOR(100, 7, char *)

struct vma_write_message
{
	int pid;
	unsigned long vm_end;
	unsigned long vm_start;
	unsigned long vm_flags;
	unsigned long vm_page_prot;
	unsigned long addr;
	unsigned long end;
};

struct pte_write_message
{
	int pid;
	unsigned long long addr;
	unsigned char data[4096];
};

struct pid_only
{
	int pid;
};
struct checkpoint_msg
{
	unsigned long long vma_address;
	char data[4096];
};
struct pid_pop
{
	int pid;
	int size;
	int return_size;
	int set_ckpt_inprogress;
	struct checkpoint_msg * buf;

};
struct pid_file_message
{
	int pid;
	char file_path[1024];
};

struct pid_list
{
	int size;
	int pids[16];
	struct user_regs_struct user_regs_struct[16];
	int pid_status[16];
};

#define SIZE_CPKT_BUF 128
struct checkpoint_msg ckpt_buf[SIZE_CPKT_BUF];

static inline unsigned long _tsc_read(void)
{
	union {
		struct  {
			unsigned int lo;
			unsigned int hi;
		};
		unsigned long  value;
	}  rv;

	/* rdtsc & cpuid clobbers eax, ebx, ecx and edx registers */
	__asm__ volatile (/* serialize */
		"xorl %%eax,%%eax;\n\t"
		"cpuid;\n\t"
		:
		:
		: "%eax", "%ebx", "%ecx", "%edx"
		);
	/*
	 * We cannot use "=A", since this would use %rax on x86_64 and
	 * return only the lower 32bits of the TSC
	 */
	__asm__ volatile ("rdtsc" : "=a" (rv.lo), "=d" (rv.hi));


	return rv.value;
}


/* Functions for the ioctl calls */


void ioctl_pid_msg(int file_desc, pid_t pid)
{
  int ret_val;
  struct pid_only pid_only;
  pid_only.pid = pid;
  ret_val = ioctl(file_desc, IOCTL_PID_MSG, &pid_only);

  if (ret_val < 0) {
    printf ("IOCTL_PID_MSG failed:%d\n", ret_val);
    exit(-1);
  }
}

void ioctl_get_regs_msg(int file_desc, int num_pids,pid_t * pid)
{
  int ret_val;
  struct pid_list pid_list;
  while(num_pids > 0)
  {
	  pid_list.size = num_pids > 16 ? 16 : num_pids;
	  memcpy(pid_list.pids,pid,sizeof(pid_t)*pid_list.size);

	  ret_val = ioctl(file_desc, IOCTL_GET_REGS, &pid_list);

	  if (ret_val < 0) {
		printf ("IOCTL_GET_REGS failed:%d\n", ret_val);
		exit(-1);
	  }
	  pid += pid_list.size;
	  num_pids -= pid_list.size;
  }
}

int ioctl_pop_msg(int file_desc, pid_t pid,struct checkpoint_msg * buf,int size_buf,int set_ckpt_inprogress)
{
  int ret_val;
  struct pid_pop pid_pop;
  pid_pop.size = size_buf;
  pid_pop.buf = buf;
  pid_pop.return_size = 0;
  pid_pop.pid = pid;
  pid_pop.set_ckpt_inprogress = set_ckpt_inprogress;
  ret_val = ioctl(file_desc, IOCTL_POP_QUEUE, &pid_pop);

  if (ret_val < 0) {
    printf ("IOCTL_POP_QUEUE failed:%d\n", ret_val);
    exit(-1);
  }

  /*if(pid_pop.return_size > 0)
  {
	  printf ("IOCTL_POP_QUEUE got :%d\n", pid_pop.return_size);
  }
  */
  return pid_pop.return_size;
}

void ioctl_monitor_msg(int file_desc, pid_t pid, char *message)
{
  int ret_val;
  struct pid_file_message pid_file_message;
  memset((void*)&pid_file_message,0, sizeof(struct pid_file_message));
  strcpy(pid_file_message.file_path,message);
  pid_file_message.pid = pid;
  ret_val = ioctl(file_desc, IOCTL_MONITOR_MSG, &pid_file_message);

  if (ret_val < 0) {
    printf ("ioctl_set_msg failed:%d\n", ret_val);
    exit(-1);
  }
}

void ioctl_replay_msg(int file_desc, char *message)
{
  int ret_val;

  ret_val = ioctl(file_desc, IOCTL_REPLAY_MSG, message);

  if (ret_val < 0) {
    printf ("ioctl_set_msg failed:%d\n", ret_val);
    exit(-1);
  }
}

void ioctl_checkpoint_msg(int file_desc, pid_t pid, char *message)
{
	int ret_val;
	struct pid_file_message pid_file_message;
	memset((void*)&pid_file_message,0, sizeof(struct pid_file_message));
	strcpy(pid_file_message.file_path,message);
	pid_file_message.pid = pid;
	ret_val = ioctl(file_desc, IOCTL_CHECKPOINT_MSG, &pid_file_message);

  if (ret_val < 0) {
    printf ("ioctl_set_msg failed:%d\n", ret_val);
    exit(-1);
  }
}

void ioctl_write_pte_msg(int file_desc, struct pte_write_message *message)
{
  int ret_val;

  ret_val = ioctl(file_desc, IOCTL_WRITE_PTE_MSG, message);

  if (ret_val < 0) {
    printf ("ioctl_set_msg failed:%d\n", ret_val);
    exit(-1);
  }
}

void ioctl_write_vma_msg(int file_desc, struct vma_write_message *message)
{
  int ret_val;

  ret_val = ioctl(file_desc, IOCTL_WRITE_VMA_MSG, message);

  if (ret_val < 0) {
    printf ("ioctl_set_msg failed:%d\n", ret_val);
    exit(-1);
  }
}
struct pte_save
{
	unsigned long long addr;
	int dirty;
	char data[4096];
};

struct vma_save
{
	unsigned long vm_end;
	unsigned long vm_start;
	unsigned long vm_flags;
	unsigned long vm_page_prot;
	unsigned long addr;
	unsigned long end;

	rb_red_blk_tree *pte_tree;
};

int
cmp_vma_save (void * a, void * b) {
    struct vma_save *va = (struct vma_save *) a;
    struct vma_save *vb = (struct vma_save *) b;
    return va->vm_start == vb->vm_start;
}

int
cmp_pte_save (void * a, void * b) {
    struct pte_save *va = (struct pte_save *) a;
    struct pte_save *vb = (struct pte_save *) b;
    return (va->addr == vb->addr);
}
rb_red_blk_tree *vma_tree;

int read_checkpoint_file(char * file_path,int incremental)
{

	rb_red_blk_node * node = NULL;
	struct vma_save temp_vma;
	struct pte_save temp_pte;
	struct vma_save * found_vma = NULL;
	struct pte_save * found_pte = NULL;

	char cbuf[1024];
	unsigned long long zero = 0, rpte = 0;
	int i  = 0;

	unsigned long raddr,rend;
	FILE * f = fopen(file_path,"r");
	if(!f) return 0;
	while(1)
	{
		if(!fread((char*)&temp_vma,sizeof(unsigned long)*6,1,f)) goto close_file;
		node = RBExactQuery(vma_tree,&temp_vma);
		if(node)
			found_vma = (struct vma_save*)node->key;
		else
			found_vma = NULL;
		printf("Replaying addr 0x%lX \n", (unsigned long)raddr);
		printf("\t end 0x%lX\n", (unsigned long)rend);

		raddr = temp_vma.addr;
		rend = temp_vma.end;

		for (; raddr != rend; raddr += 4096) {

			if(!fread((char*)&rpte,sizeof(unsigned long long),1,f)) goto close_file;

			if (!rpte)
			{
				printf("Replay skipping PTE 0x%lx\n",raddr);
				continue;
			}


			if(!found_vma)
			{
				found_vma = malloc(sizeof(struct vma_save));
				found_vma->vm_start = temp_vma.vm_start;
				memcpy(found_vma,&temp_vma,sizeof(struct vma_save));
				found_vma->pte_tree = RBTreeCreate(cmp_pte_save,
					     free,
					     NULL,
					     printf,
					     printf);
				RBTreeInsert(vma_tree, found_vma,found_vma);
			}



			temp_pte.addr = raddr;
			temp_pte.dirty = 0;

			printf("Replaying pte for 0x%lX :Replay 0x%lx\n",raddr,rpte);
			if(!fread(temp_pte.data, 4096,1,f)) goto close_file;
			printf("Searching...\n");
			node = RBExactQuery(found_vma->pte_tree,&temp_pte);
					if(node)
						found_pte = (struct pte_save *)node->key;
					else
						found_pte = NULL;

			if(!found_pte)
			{
				printf("Not Found...\n");
				found_pte = malloc(sizeof(struct pte_save));
				found_pte->addr = raddr;
				memcpy(found_pte,&temp_pte,sizeof(struct pte_save));
				RBTreeInsert(found_vma->pte_tree, found_pte,found_pte);
				found_pte->dirty = 1;
			}
			else
			{
				printf("Found...\n");
				memcpy(found_pte,&temp_pte,sizeof(struct pte_save));
				found_pte->dirty = incremental;
			}

		}

	}
close_file:
	fclose(f);
}
int keepRunning = 1;
void  INThandler(int sig)
{
	keepRunning = 0;
}

int gettids(int pid, int * tid_buf)
{
	int i = 0;
	DIR *proc_dir;
	{
		char dirname[100];
		snprintf(dirname, sizeof dirname, "/proc/%d/task", pid);
		proc_dir = opendir(dirname);
	}

	if (proc_dir)
	{
		/* /proc available, iterate through tasks... */
		struct dirent *entry;
		while ((entry = readdir(proc_dir)) != NULL)
		{
			if(entry->d_name[0] == '.')
				continue;

			tid_buf[i++] = atoi(entry->d_name);

			/* ... (do stuff with tid) ... */
		}

		closedir(proc_dir);
	}
	else
	{
		return 0;
		/* /proc not available, act accordingly */
	}
	return i;
}



/* Main - Call the ioctl functions */
int main(int argc, char * argv[])
{
  int file_desc, ret_val;
  unsigned long start,vma_time,reg_time,pop_time;
  int tid_buf[1024];
  memset(tid_buf,0,sizeof(int)*1024);
  int numtid = 0;

  char buf[1024];
  FILE * file = NULL;
  memset(buf,0,1024);
  file_desc = open(DEVICE_FILE_NAME, 0);
  if (file_desc < 0) {
    printf ("Can't open device file: %s\n",
            DEVICE_FILE_NAME);
    exit(-1);
  }

  signal(SIGINT, INThandler);

  if(argc > 2)
  {
	  if(!strcmp(argv[1],"monitor"))
  	  {
	    pid_t pid = atoi(argv[2]);

	    ioctl_pid_msg(file_desc,pid);
		sprintf(buf,"%s.vma",argv[3]);
		file = fopen(buf,"w");
		if(file)
			fclose(file);
		else
			return -1;

	    ioctl_monitor_msg(file_desc,pid,buf);
  	  }
	  else if(!strcmp(argv[1],"pop"))
  	  {
	    pid_t pid = atoi(argv[2]);

	    ioctl_pop_msg(file_desc,pid,ckpt_buf,SIZE_CPKT_BUF,0);
  	  }
	  else if(!strcmp(argv[1],"concurrent"))
	  {
			pid_t pid = atoi(argv[2]);
			ioctl_pid_msg(file_desc,pid);
			sprintf(buf,"%s.vma",argv[3]);
			file = fopen(buf,"w");
			if(file)
				fclose(file);
			else
				return -1;
			int sizeofpop = atoi(argv[4]);
			struct checkpoint_msg* pvt_ckpt_buf = (struct checkpoint_msg*)malloc(sizeof(struct checkpoint_msg)*sizeofpop);
			numtid = gettids(pid,tid_buf);
			struct user_regs_struct regs;
			struct user_fpregs_struct fpregs;
			int status;

			printf("num tids %d\n",numtid);
			for (int i = 0; i < numtid; i++)
			{

			  ptrace(PTRACE_ATTACH, tid_buf[i],
							 NULL, NULL);
			  waitpid(tid_buf[i],&status,0);

			}
			ioctl_checkpoint_msg(file_desc,pid,buf);
			sprintf(buf,"%s.regs",argv[3]);
			file = fopen(buf,"w");
			if(file)
			{
			  fwrite(&numtid,sizeof(int),1,file);
			  for (int i = 0; i < numtid; i++)
				  fwrite(&tid_buf[i],sizeof(int),1,file);
			  for (int i = 0; i < numtid; i++)
			  {

				  ptrace(PTRACE_GETREGS, tid_buf[i],
						 NULL, &regs);
				  ptrace(PTRACE_GETFPXREGS, tid_buf[i],
								 NULL, &fpregs);
			//			  unsigned long ins = ptrace(PTRACE_PEEKTEXT, pid,
			//						   regs.rip, NULL);
			//			  printf("EIP: %lx Instruction executed: %lx\n",
			//					 regs.rip, ins);

				  fwrite(&regs,sizeof(struct user_regs_struct),1,file);
				  fwrite(&fpregs,sizeof(struct user_fpregs_struct),1,file);

			  }
			  fclose(file);

			}
			else
			{
			  printf("Failed to open %s",buf);
			}

			for (int i = 0; i < numtid; i++)
			{
			ptrace(PTRACE_DETACH, tid_buf[i],
								NULL, NULL);
			}
			printf("Running concurrent checkpoint\n");
			int sleep_count = 0;
			while(keepRunning)
			{

				start = _tsc_read();
				int return_size = ioctl_pop_msg(file_desc,pid,pvt_ckpt_buf,sizeofpop,0);
				pop_time =_tsc_read() - start;
				if(return_size > 0)
					printf("pop time = %d per page and %d pages\n",pop_time/return_size,return_size);
				if(return_size < sizeofpop)
				{
					sleep_count++;
					usleep(1000);
				}

				if(sleep_count > 100)
				{
					sleep_count = 0;
					printf("Setting ckpt_inprogress\n");
					return_size = 1;
					while(return_size)
						return_size = ioctl_pop_msg(file_desc,pid,pvt_ckpt_buf,sizeofpop,1);
					//usleep(10000000);
					ioctl_get_regs_msg(file_desc,numtid,tid_buf);
					printf("Clearing ckpt_inprogress\n");
					return_size = ioctl_pop_msg(file_desc,pid,pvt_ckpt_buf,sizeofpop,0);
				}
				start = _tsc_read();
			}
			printf("Exiting concurrent Checkpoint\n");
	  }
	  else if(!strcmp(argv[1],"checkpoint"))
	  {
		  start = _tsc_read();
		  pid_t pid = atoi(argv[2]);
		  ioctl_pid_msg(file_desc,pid);
		  numtid = gettids(pid,tid_buf);

		  sprintf(buf,"%s.vma",argv[3]);
		  file = fopen(buf,"w");
		  if(file)
			  fclose(file);
		  else
			  return -1;



		  struct user_regs_struct regs;
		  struct user_fpregs_struct fpregs;
		  int status;

		  printf("num tids %d\n",numtid);
		  for (int i = 0; i < numtid; i++)
		  {

			  ptrace(PTRACE_ATTACH, tid_buf[i],
							 NULL, NULL);
			  waitpid(tid_buf[i],&status,0);

		  }

		  ioctl_checkpoint_msg(file_desc,pid,buf);
		  vma_time = _tsc_read() - start;



		  sprintf(buf,"%s.regs",argv[3]);
		  file = fopen(buf,"w");
		  if(file)
		  {
			  fwrite(&numtid,sizeof(int),1,file);
			  for (int i = 0; i < numtid; i++)
				  fwrite(&tid_buf[i],sizeof(int),1,file);
			  for (int i = 0; i < numtid; i++)
			  {

				  ptrace(PTRACE_GETREGS, tid_buf[i],
						 NULL, &regs);
				  ptrace(PTRACE_GETFPXREGS, tid_buf[i],
								 NULL, &fpregs);
	//			  unsigned long ins = ptrace(PTRACE_PEEKTEXT, pid,
	//						   regs.rip, NULL);
	//			  printf("EIP: %lx Instruction executed: %lx\n",
	//					 regs.rip, ins);

				  fwrite(&regs,sizeof(struct user_regs_struct),1,file);
				  fwrite(&fpregs,sizeof(struct user_fpregs_struct),1,file);

			  }
			  fclose(file);

		  }
		  else
		  {
			  printf("Failed to open %s",buf);
		  }

		  for (int i = 0; i < numtid; i++)
		  {
			ptrace(PTRACE_DETACH, tid_buf[i],
				  				NULL, NULL);
		  }




		  reg_time =_tsc_read() - vma_time;
		  printf("VMA Dump Time %u\nREG Dump Time %u\n",vma_time,reg_time);


	  }
	  else if(!strcmp(argv[1],"replay"))
	  {
		  printf("Replay\n");
		  pid_t pid = atoi(argv[2]);
		  ioctl_pid_msg(file_desc,pid);
		  sprintf(buf,"%s.vma",argv[3]);


		  struct user_regs_struct regs;
		  struct user_fpregs_struct fpregs;
		  int status;

		  ptrace(PTRACE_ATTACH, pid,
				 NULL, NULL);
		  waitpid(pid,&status,0);

		  ioctl_replay_msg(file_desc,buf);

		  sprintf(buf,"%s.regs",argv[3]);
		  file = fopen(buf,"r");
		  if(file)
		  {

			  fread(&regs,sizeof(struct user_regs_struct),1,file);
			  fread(&fpregs,sizeof(struct user_fpregs_struct),1,file);
			  ptrace(PTRACE_SETREGS, pid,
					 NULL, &regs);
			  ptrace(PTRACE_SETFPXREGS, pid,
							 NULL, &fpregs);
			  unsigned long ins = ptrace(PTRACE_PEEKTEXT, pid,
						   regs.rip, NULL);
			  printf("EIP: %lx Instruction executed: %lx\n",
					 regs.rip, ins);

			  fclose(file);
		  }
		  else
		  {
			  printf("Failed to open %s",buf);
		  }
		  printf("Sleeeping 5 before resuming\n");
		  sleep(5);
		  ptrace(PTRACE_DETACH, pid,
				 NULL, NULL);
	  }
	  else if(!strcmp(argv[1],"replay_fork"))
	  {
		  pid_t pid = fork();
		  if(pid != 0)
		  {
			  printf("Replay\n");

			  sprintf(buf,"%s.vma",argv[2]);


			  struct user_regs_struct regs;
			  struct user_fpregs_struct fpregs;
			  int status;

			  ptrace(PTRACE_ATTACH, pid,
					 NULL, NULL);
			  waitpid(pid,&status,0);

			  ioctl_replay_msg(file_desc,buf);

			  sprintf(buf,"%s.regs",argv[3]);
			  file = fopen(buf,"r");
			  if(file)
			  {

				  fread(&regs,sizeof(struct user_regs_struct),1,file);
				  fread(&fpregs,sizeof(struct user_fpregs_struct),1,file);
				  ptrace(PTRACE_SETREGS, pid,
						 NULL, &regs);
				  ptrace(PTRACE_SETFPXREGS, pid,
								 NULL, &fpregs);
				  unsigned long ins = ptrace(PTRACE_PEEKTEXT, pid,
							   regs.rip, NULL);
				  printf("EIP: %lx Instruction executed: %lx\n",
						 regs.rip, ins);

				  fclose(file);
			  }
			  else
			  {
				  printf("Failed to open %s",buf);
			  }

			  ptrace(PTRACE_DETACH, pid,
					 NULL, NULL);
		  }else
		  {
			  while(1)
			  {
				  printf("Sleeping waiting for replay\n");
				  sleep(1);
			  }
		  }
	  }
	  else if(!strcmp(argv[1],"replay_file"))
	  {
		  printf("Replay\n");
		  int status;
		  id_t pid = atoi(argv[2]);
		  ioctl_pid_msg(file_desc,pid);
		  strcpy(buf,argv[3]);
		  vma_tree = RBTreeCreate(cmp_vma_save,
				     free,
				     NULL,
				     printf,
				     printf);
		  sprintf(buf,"%s.vma",argv[3]);
		  read_checkpoint_file(buf,0);
		  int filenum = 3;
		  for(filenum = 4; filenum < argc; filenum++)
		  {
			  printf("-----Incremental Replay------------\n");
			  sprintf(buf,"%s.vma",argv[filenum]);
			  read_checkpoint_file(buf,1);
		  }

		  sprintf(buf,"%s.regs",argv[filenum-1]);

		  file = fopen(buf,"r");
		  if(file)
		  {

			  int tid;
			  fread(&numtid,sizeof(int),1,file);
			  for (int i = 0; i < numtid; i++)
			  {
				  fread(&tid,sizeof(int),1,file);
				  tid_buf[i] = tid;
				  ptrace(PTRACE_ATTACH, tid,
									 NULL, NULL);
				  waitpid(tid,&status,0);
			  }

			  struct rb_red_blk_node* vma_node = vma_tree->root;
			  vma_node->search = 0;
			  while(vma_node)
			  {
				  struct vma_save * found_vma = (struct vma_save *)vma_node->info;
				  if(found_vma)
				  {
					  struct rb_red_blk_node* pte_node = found_vma->pte_tree->root;
					  pte_node->search = 0;
					  while(pte_node)
					  {
						  struct pte_save * found_pte = (struct pte_save *)pte_node->info;
						  if(found_pte)
						  {
							  if(found_pte->dirty > 0 || found_vma->vm_flags & 0x2)
							  {
								  struct pte_write_message pte_msg;
								  printf("Writing PTE at 0x%llX\n",found_pte->addr);
								  pte_msg.addr = found_pte->addr;
								  memcpy(pte_msg.data,found_pte->data,4096);

								  ioctl_write_pte_msg(file_desc,&pte_msg);
							  }
						  }
						  pte_node = RBTreeIterate(pte_node);
					  }
				  }

				  vma_node = RBTreeIterate(vma_node);
			  }

			  for (int i = 0; i < numtid; i++)
			  {

				  printf("replaying tid %d\n",tid_buf[i]);


				  struct user_regs_struct regs;
				  struct user_fpregs_struct fpregs;
				  fread(&regs,sizeof(struct user_regs_struct),1,file);
				  fread(&fpregs,sizeof(struct user_fpregs_struct),1,file);
				  ptrace(PTRACE_SETREGS, tid_buf[i],
						 NULL, &regs);
				  ptrace(PTRACE_SETFPXREGS, tid_buf[i],
								 NULL, &fpregs);
				  unsigned long ins = ptrace(PTRACE_PEEKTEXT, tid_buf[i],
							   regs.rip, NULL);
				  printf("EIP: %lx Instruction executed: %lx\n",
						 regs.rip, ins);


			  }
			  fclose(file);
		  }
		  else
		  {
			  printf("Failed to open %s\n",buf);
		  }

		  printf("Sleeeping 5 before resuming\n");
		  sleep(5);

		  for (int i = 0; i < numtid; i++)
		  {
			  ptrace(PTRACE_DETACH, tid_buf[i],
			  				 NULL, NULL);
		  }


	  }
	  else if(!strcmp(argv[1],"ptrace"))
	  {
		  printf("Ptrace\n");
		  pid_t pid = atoi(argv[2]);

		  struct user_regs_struct regs;
		  struct user_fpregs_struct fpregs;

		  ptrace(PTRACE_ATTACH, pid,
				 NULL, NULL);
		  wait(NULL);
		  ptrace(PTRACE_GETREGS, pid,
				 NULL, &regs);
		  ptrace(PTRACE_GETFPXREGS, pid,
		  				 NULL, &fpregs);
		  unsigned long ins = ptrace(PTRACE_PEEKTEXT, pid,
					   regs.rip, NULL);
		  printf("EIP: %lx Instruction executed: %lx\n",
				 regs.rip, ins);
		  ptrace(PTRACE_DETACH, pid,
				 NULL, NULL);

		  printf(" r15 0x%llX\n",regs.r15);
		  printf(" r14 0x%llX\n",regs.r14);
		  printf(" r13 0x%llX\n",regs.r13);
		  printf(" r12 0x%llX\n",regs.r12);
		  printf(" rbp 0x%llX\n",regs.rbp);
		  printf(" rbx 0x%llX\n",regs.rbx);
		  printf(" r11 0x%llX\n",regs.r11);
		  printf(" r10 0x%llX\n",regs.r10);
		  printf(" r9 0x%llX\n",regs.r9);
		  printf(" r8 0x%llX\n",regs.r8);
		  printf(" rax 0x%llX\n",regs.rax);
		  printf(" rcx 0x%llX\n",regs.rcx);
		  printf(" rdx 0x%llX\n",regs.rdx);
		  printf(" rsi 0x%llX\n",regs.rsi);
		  printf(" rdi 0x%llX\n",regs.rdi);
		  printf(" orig_rax 0x%llX\n",regs.orig_rax);
		  printf(" rip 0x%llX\n",regs.rip);
		  printf(" cs 0x%llX\n",regs.cs);
		  printf(" eflags 0x%llX\n",regs.eflags);
		  printf(" rsp 0x%llX\n",regs.rsp);
		  printf(" ss 0x%llX\n",regs.ss);
		  printf(" fs_base 0x%llX\n",regs.fs);
		  printf(" gs_base 0x%llX\n",regs.gs);
		  printf(" ds 0x%llX\n",regs.ds);
		  printf(" es 0x%llX\n",regs.es);
		  printf(" fs 0x%llX\n",regs.fs);
		  printf(" gs 0x%llX\n",regs.gs);

	  }


  }
  //ioctl_get_msg(file_desc);
  //ioctl_set_msg(file_desc, msg);

  close(file_desc);

  //RBTreeDestroy(vma_tree);
}

