SRC := red_black_tree.cpp stack.cpp misc.cpp rt_ckpt_driver.cpp rt_ckpt_mngr.cpp

OBJS=$(subst .c,.o,$(subst .cpp,.o,$(SRC)))

CFLAGS += -g
CXXFLAGS += -g -mtune=atom

%.o: %.c %.cpp
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

all : $(OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o rt_ckpt $(OBJS) $(LDLIBS)

clean:			
	rm -f *.o *~ rt_ckpt

